from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd

class XGBoostTransform(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        self.Y = y
        return self

    def transform(self, X):
        data = X.copy()
        for coluna in data.columns:
            if type(data[coluna].values[0]) == str:
                data[coluna] = pd.Categorical(data[coluna]).codes
        return data

class DummiesColumns(BaseEstimator, TransformerMixin):
    def __init__(self,columns):
        self.columns = columns

    def fit(self, X, y=None):
        return self

    def transform(self,X):
        data = X.copy()
        return pd.get_dummies(data, columns=self.columns)

class DropColumns(BaseEstimator, TransformerMixin):
    def __init__(self, columns):
        self.columns = columns

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        data = X.copy()
        return data.drop(labels=self.columns, axis='columns')